$(function(){
	/**
	*文件选择组件支持用户手工输入路径和通过选择按钮进行选择
	*此处为文件选择组件的选择按钮（label）绑定了一个文件选择input,并在文件选择完成后将选择文件路径写入文件路径输入框。
	*/
	$("div.file-input").each(function(_,item){
		var accept=$(this).data("accept");	//接受的文件类型
		var multiple=$(this).data("multiple"); //选择多个文件
		var dir=$(this).data("dir"); //选择文件夹
		//创建文件选择Input
		var fileInputId="file-input-"+parseInt(Math.random() * 10000).toString();
		var fileInput=$('<input id="'+fileInputId+'"'
			+(!!accept?' accept="'+accept+'"':'')
			+((multiple=='multiple')?' multiple':'')
			+((dir=='dir')?' nwdirectory':'')
			+' type="file" />').hide();
		
		//绑定文件选择input到选择按钮
		$(item).find('label')
			.attr("for",fileInputId)
			.append(fileInput);

		//同步选择文件到输入框并触发输入框的change事件
		fileInput.change(function(){
			$(item).find('input[type="text"]').val($(this).val()).trigger('change');
		});
	});
});